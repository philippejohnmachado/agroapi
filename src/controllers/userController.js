'use strict';

const mongoose = require('mongoose');
const userModel = require('../models/userModel');
const validationContract = require('../validators/validation');
const repository = require('../repositories/userRepository');
const Decrypt = require('../crypt/validationsenha');
const Encrypt = require('../crypt/encrypt');
const emailservice = require('../services/email-services');
const authService = require('../services/auth-services');

exports.get =  async(req, res, next) =>{
    try{
        var data = await repository.get(req.params.email);
        res.status(200).send(data);
    }catch (e){
        res.status(500).send({
            message: 'Falha ao processar  sua informação.'
        })
    }
 
}

exports.NovaSenha =  async(req, res, next) =>{
    let contract = new  validationContract();
        contract.isEmail(req.body.email,'E-mail invalido');
    try{
        var data = await repository.get(req.params.email);
        
        if(data != null){
            const max = 9;
            const min = 0;
            var senha = ''; 

            for(var i = 0; i < 6; i++){
               var s = Math.floor(Math.random()*(max - min))+ min;
               senha = senha+s.toString();
            };

           
           var senhaCrypt =  await Encrypt.crypt(senha);

        //    res.status(200).send(senhaCrypt.toString());
        //    return;

           
           await repository.updateSenhaProvisoria(data.id,senhaCrypt.toString());

          await emailservice.send(
            req.params.email,
            'Recuperação de senha AgroInfo',
            'Olá, <strong>'+data.nome+'</strong>, Sua nova senha provisoria é: <strong>'+senha+'</strong>, Acesse o aplicativo para redefinir a senha'
             );

             res.status(200).send({
                message: 'Nova senha provisoria gerada com sucesso',
                result: true,
                error: false
            });

        }
        else{
            res.status(200).send({
                message: 'Usuario nao encontrado',
                result: false,
                error: false
            });
        }
    }catch (e){
        res.status(500).send({
            message: 'Falha ao processar  sua informação.'+e
        })
    }
 
}

exports.UserValidacao = async(req, res, next)=>{
     let contract = new  validationContract();
        contract.isEmail(req.body.email,'E-mail invalido');
        contract.isRequired(req.body.senha,'Senha invalida');
        try{

            var response = await repository.get(req.body.email);
            
            var senhaDB = await Decrypt.decrypt(response.senha);
            var senhaUser = await Decrypt.decrypt(req.body.senha);

            if(senhaDB === senhaUser){

                
               const token = await authService.generateToken({
                   email: response.email,
                    senha: senhaDB});
                  
                res.status(200).send({
                    UserId: response.id,
                    token: token,
                    message: 'Usuario válido',
                    SenhaProv: response.senhaProvi,
                    result: true,
                    error: false
                });
            }else{
                res.status(200).send({
                    token: '',
                    message: 'E-mail ou senha invalidos.',
                    result: false,
                    error: false
                });
            }

        }catch(e){
            res.status(500).send({
                message: 'Falha ao processar sua informção:' + e,
                result: false,
                error: true
            });
        }    

    }

exports.post = async(req, res, next) =>{
        let contract = new  validationContract();
        contract.isEmail(req.body.email,'E-mail invalido');
        
        if(!contract.isValid()){
            res.status(200).send({
                mensage: contract.errors(),
                result: false,
                error: true, }
                ).end();
            return;
        }
        try{

            await repository.creat(req.body);
          
            emailservice.send(
                req.body.email,
                'Bem vindo ao Agroinfo',
                 global.EMAIL_TMPL.replace('{0}',req.body.nome)
                 );

            res.status(201).send({
                message: 'Usuario cadastrado com sucesso.',
                result: true,
                error: false
            });
        }catch(e){
            res.status(500).send({
                message: 'Falha ao processar  sua informação.',
                result: false,
                error: true
            });
        }
};

exports.UpdateNovaSenha = async(req, res, next) =>{
    try{
        await repository.updateNovaSenha(req.body);

        res.status(200).send({
            message: 'Senha Atualizada com sucesso',
            result: true,
            error: false
        });
    }catch(e){
        res.status(400).send({
            message: 'Falha ao atualizar a senha',
            result: false,
            error: true
        });
    };
};

exports.put = (req, res, next) =>{
    repository.update(req.params.id,req.body).then(x => {
        res.status(200).send({
            message: 'Usuario atualizado com sucesso.'
        });
    }).catch(e =>{
        res.status(400).send({
            message: 'Falha ao atualizar o usuario',
            data: e
        });
    });
};

// exports.delete = (req, res, next) =>{
//     res.status(201).send(req.body);
// };