'use strict'

//importação de dependencias
const express = require('express'); // bibliotecas de mvc
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const config = require('./config');

//conecta com o banco
mongoose.connect(config.connectionString);

//criando o server
const app = express();

//Carregar as rotas
const indexRoutes = require('./routes/indexRoute');
const userRoutes = require('./routes/userRoute');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


app.use('/', indexRoutes);
app.use('/user', userRoutes);


module.exports = app;